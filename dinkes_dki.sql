-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2021 at 06:19 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dinkes_dki`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_kecamatan`
--

CREATE TABLE `table_kecamatan` (
  `id_kecamatan` int(11) NOT NULL,
  `nama_kecamatan` varchar(255) NOT NULL,
  `id_kota` int(11) NOT NULL,
  `nama_kota` varchar(255) NOT NULL,
  `id_provinsi` int(11) NOT NULL,
  `nama_provinsi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_kecamatan`
--

INSERT INTO `table_kecamatan` (`id_kecamatan`, `nama_kecamatan`, `id_kota`, `nama_kota`, `id_provinsi`, `nama_provinsi`) VALUES
(317105, 'Kemayoran', 3171, 'Jakarta Pusat', 31, 'DKI Jakarta'),
(317203, 'Pasar Rebo', 3172, 'Jakarta Timur', 31, 'DKI Jakarta'),
(317401, 'Setiabudi', 3174, 'Jakarta Selatan', 31, 'DKI Jakarta');

-- --------------------------------------------------------

--
-- Table structure for table `table_puskesmas`
--

CREATE TABLE `table_puskesmas` (
  `id_puskes` varchar(20) NOT NULL,
  `nama_puskesmas` varchar(255) NOT NULL,
  `alamat_puskesmas` varchar(255) NOT NULL,
  `no_telp_puskesmas` varchar(20) NOT NULL,
  `kode_kecamatan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_puskesmas`
--

INSERT INTO `table_puskesmas` (`id_puskes`, `nama_puskesmas`, `alamat_puskesmas`, `no_telp_puskesmas`, `kode_kecamatan`) VALUES
('P31710002', 'Puskesmas Kecamatan Kemayoran', 'Kp. Sukasari No. 13', '021-002002', 317105),
('P31720003', 'Puskesmas Kecamatan Pasar Rebo', 'Jl. Kalisari No. 10', '021-003003', 317203);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `table_kecamatan`
--
ALTER TABLE `table_kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indexes for table `table_puskesmas`
--
ALTER TABLE `table_puskesmas`
  ADD PRIMARY KEY (`id_puskes`),
  ADD KEY `kode_kecamatan` (`kode_kecamatan`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `table_puskesmas`
--
ALTER TABLE `table_puskesmas`
  ADD CONSTRAINT `table_puskesmas_ibfk_1` FOREIGN KEY (`kode_kecamatan`) REFERENCES `table_kecamatan` (`id_kecamatan`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
