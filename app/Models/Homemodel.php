<?php

namespace App\Models;

use CodeIgniter\Model;

class Homemodel extends Model
{
    protected $db;
    protected $builder;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
    }

    public function getPuskesmas($idKecamatan = false)
    {
        if ($idKecamatan != false) {
            $this->builder = $this->db->table('table_puskesmas');
            $this->builder->select('*');
            $this->builder->join('table_kecamatan', 'table_kecamatan.id_kecamatan = table_puskesmas.kode_kecamatan');
            $this->builder->where(['kode_kecamatan' => $idKecamatan]);
            $query = $this->builder->get();

            return $query->getResult();
        } else {
            $this->builder = $this->db->table('table_puskesmas');
            $this->builder->select('*');
            $this->builder->join('table_kecamatan', 'table_kecamatan.id_kecamatan = table_puskesmas.kode_kecamatan');
            $query = $this->builder->get();
            return $query->getResult();
        }
    }

    public function insertPuskesmas($data)
    {
        $this->builder = $this->db->table('table_puskesmas');
        $this->builder->ignore(true)->insertBatch($data);
    }

    public function insertKecamatan($data)
    {
        $this->builder = $this->db->table('table_kecamatan');
        $this->builder->ignore(true)->insertBatch($data);
    }
}
