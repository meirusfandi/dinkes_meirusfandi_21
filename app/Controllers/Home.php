<?php

namespace App\Controllers;

use App\Models\Homemodel;
use CodeIgniter\RESTful\ResourceController;

class Home extends ResourceController
{
	protected $model;

	public function __construct()
	{
		$this->model = new Homemodel();
	}

	public function index()
	{
		set_time_limit(0);

		// set server and base uri
		$client = \Config\Services::curlrequest([
			'baseURI' => 'https://dinkes.jakarta.go.id/api_rekrutmen/'
		]);

		// get data kecamatan
		$kec = $client->request('get', 'kecamatan', [
			'headers' => [
				'Authorization' => 'rahasia'
			]
		]);

		$body_kec = json_decode($kec->getBody());
		if ($body_kec->code == 200) {
			// insert data kecamatan to db
			$data_kec = [];
			foreach ($body_kec->data as $key => $value) {
				$each = [];
				$each['id_kecamatan'] = $value->id_kecamatan;
				$each['nama_kecamatan'] = $value->nama_kecamatan;
				$each['id_kota'] = $value->id_kota;
				$each['nama_kota'] = $value->nama_kota;
				$each['id_provinsi'] = $value->id_provinsi;
				$each['nama_provinsi'] = $value->nama_provinsi;
				array_push($data_kec, $each);
			}

			$this->model->insertKecamatan($data_kec);
		}

		// get data puskes
		$puskes = $client->request('get', 'puskesmas', [
			'headers' => [
				'Authorization' => 'rahasia'
			]
		]);

		$body = json_decode($puskes->getBody());
		// check if data found
		if ($body->code == 200) {
			// insert data puskesmas to db
			$data_puskes = [];
			foreach ($body->data as $key => $value) {
				$each = [];
				$each['id_puskes'] = $value->id_puskes;
				$each['nama_puskesmas'] = $value->nama_puskesmas;
				$each['alamat_puskesmas'] = $value->alamat_puskesmas;
				$each['no_telp_puskesmas'] = $value->no_telp_puskesmas;
				$each['kode_kecamatan'] = $value->kode_kecamatan;
				array_push($data_puskes, $each);
			}
			$this->model->insertPuskesmas($data_puskes);
		}

		$puskesmas = $this->model->getPuskesmas();

		$data_puskesmas = [];
		foreach ($puskesmas as $key => $value) {
			$kota = [
				'id_kota' => $value->id_kota,
				'nama_kota' => $value->nama_kota
			];
			$provinsi = [
				'id_provinsi' => $value->id_provinsi,
				'nama_provinsi' => $value->nama_provinsi
			];
			$kecamatan = [
				'id_kecamatan' => $value->id_kecamatan,
				'nama_kecamatan' => $value->nama_kecamatan
			];

			$data = [
				'id_puskesmas' => $value->id_puskes,
				'nama_puskesmas' => $value->nama_puskesmas,
				'alamat_puskesmas' => $value->alamat_puskesmas,
				'no_telp_puskesmas' => $value->no_telp_puskesmas,
				'provinsi' => $provinsi,
				'kota' => $kota,
				'kecamatan' => $kecamatan
			];

			array_push($data_puskesmas, $data);
		}

		if (count($data_puskesmas) > 0) {
			return $this->respond(array(
				'status' => 'success',
				'code' => 200,
				'count' => count($data_puskesmas),
				'data' => $data_puskesmas
			));
		} else {
			return $this->respond(array(
				'status' => 'success',
				'code' => 200,
				'count' => 0,
				'data' => 'No data found!'
			));
		}
	}
}
